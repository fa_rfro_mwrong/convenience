package convenience

import (
	"testing"

	"github.com/sanity-io/litter"
)

func TestGmail(t *testing.T) {
	var gmail = new(Gmail)
	gmail.ReadRC()
	t.Log(litter.Sdump(gmail))
	if err := gmail.Send([]string{"66867242@qq.com"}, "test from golang", "hello there"); err != nil {
		t.Log(err)
		t.Error("failed to send email from gmail")
	}
}
