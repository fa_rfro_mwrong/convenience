module arestsdk

go 1.12

require (
	github.com/cheekybits/genny v1.0.0
	github.com/gen2brain/beeep v0.0.0-20190317152856-aa3d7c1499fd
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/sanity-io/litter v1.1.0
)
