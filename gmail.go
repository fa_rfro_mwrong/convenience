package convenience

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/smtp"
	"os"
	"os/user"
	"path"
	"strings"
)

type Gmail struct {
	User string `json:"user"`
	Pass string `json:"pass"`
}

// ReadEnv read gmail user and pass from environment variables
func (g *Gmail) ReadEnv() (err error) {
	user := os.Getenv("EMAIL_USER")
	pass := os.Getenv("EMAIL_PASS")
	if user == "" {
		return errors.New("failed to read env var EMAIL_USER")
	}
	if pass == "" {
		return errors.New("failed to read env var EMAIL_PASS")
	}
	g.User = user
	g.Pass = pass
	return
}

// ReadConfig read gmail configuration from config file
func (g *Gmail) ReadConfig(configSource string) (err error) {
	bytes, err := ioutil.ReadFile(configSource)
	if err != nil {
		return
	}
	return json.Unmarshal(bytes, g)
}

// ReadRC read gmail configuration from default rc file
func (g *Gmail) ReadRC() (err error) {
	user, err := user.Current()
	if err != nil {
		return
	}
	rc := path.Join(user.HomeDir, ".gmailrc")
	return g.ReadConfig(rc)
}

func (g *Gmail) Send(to []string, subj, body string) (err error) {
	msg := fmt.Sprintf(`From: %s
To: %s
Subject: %s
%s
`,
		g.User,
		strings.Join(to, ";"),
		subj,
		body,
	)
	return smtp.SendMail(
		"smtp.gmail.com:587",
		smtp.PlainAuth("", g.User, g.Pass, "smtp.gmail.com"),
		g.User,
		to,
		[]byte(msg),
	)
}
