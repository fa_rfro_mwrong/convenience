package convenience

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

// FinalURL get final url after redirection
func FinalURL(url string) (final string, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	final = resp.Request.URL.String()
	return
}

// DownloadFromURL download file from url
func DownloadFromURL(url string, fileName string, force bool) (int64, error) {
	// if fileName is empty, use the filename from url
	if fileName == "" {
		tokens := strings.Split(url, "/")
		fileName = tokens[len(tokens)-1]
	}
	if _, err := os.Stat(fileName); err == nil && !force {
		return 0, fmt.Errorf("file exists: %s", fileName)
	}
	output, err := os.Create(fileName)
	if err != nil {
		fmt.Println("Error while creating", fileName, "-", err)
		return 0, err
	}
	defer output.Close()
	response, err := http.Get(url)
	if err != nil {
		return 0, err
	}
	defer response.Body.Close()
	n, err := io.Copy(output, response.Body)
	if err != nil {
		return 0, err
	}
	return n, nil
}
