//go:generate genny -in=$GOFILE -out=gen-$GOFILE gen "Item=string,int"

package convenience

import "github.com/cheekybits/genny/generic"

// Item item type
type Item generic.Type

// ItemBao wraps an array of items
type ItemBao struct {
	Array []Item
}

// NewItemBao return a pointer to a new ItemBao
func NewItemBao(array []Item) *ItemBao {
	return &ItemBao{
		Array: array,
	}
}

// FindFirst find first index of element in array, return -1 if not found
func (bao *ItemBao) FindFirst(elem Item) int {
	for i, v := range bao.Array {
		if v == elem {
			return i
		}
	}
	return -1
}

// Find find first index of element in array, starting from offset, return -1 if not found.
func (bao *ItemBao) Find(elem Item, offset int) int {
	if offset >= len(bao.Array) {
		return -1
	}
	if offset < 0 {
		offset = 0
	}
	for i := offset; i < len(bao.Array); i++ {
		if bao.Array[i] == elem {
			return i
		}
	}
	return -1
}
